#include <GLFW/glfw3.h>

#include "Animation.hpp"

Animation::Animation(bool InitRepeat)
:DoRepeat(InitRepeat){
	
}

bool Animation::SetFrames(std::vector<unsigned int> *NewFrames){
	if(NewFrames && (!NewFrames->empty()) && !(NewFrames->size() % 2)){
		for(std::vector<unsigned int>::iterator IT = NewFrames->begin();IT != NewFrames->end();IT += 2){
			Frames.push_back(Vectu(*IT,*(IT + 1)));
		}
		
		return true;
	}
	
	return false;
}

bool Animation::SetFrames(std::vector<unsigned int> *NewFrames,unsigned int GlobalDuration){
	if(NewFrames && !NewFrames->empty()){
		for(std::vector<unsigned int>::iterator IT = NewFrames->begin();IT != NewFrames->end();++IT){
			Frames.push_back(Vectu(*IT,GlobalDuration));
		}
		
		return true;
	}
	
	return false;
}

AnimationInstance::AnimationInstance(Animation *NewAnim)
:LastUpdate(glfwGetTime()),CurrentFrame(0),Anim(NewAnim){
	
}

bool AnimationInstance::Update(){
	if(Anim && (!Anim->Frames.empty()) && glfwGetTime() - LastUpdate > Anim->Frames[CurrentFrame].Y && (Anim->DoRepeat || CurrentFrame != Anim->Frames.size() - 1)){
		CurrentFrame = (CurrentFrame + 1) % Anim->Frames.size();
		
		return true;
	}
	
	return false;
}

void AnimationInstance::Reset(){
	CurrentFrame = 0;
	LastUpdate = glfwGetTime();
}