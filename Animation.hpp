#ifndef ANIMATION_INC
	#include <vector>
	
	#include "Utils.hpp"
		
	class Animation{
		friend class AnimationInstance;
		
		private:
			std::vector<Vectu> Frames;
			
		public:
			bool DoRepeat;
			
			Animation(bool InitRepeat = true);
			
			bool SetFrames(std::vector<unsigned int> *NewFrames);
			bool SetFrames(std::vector<unsigned int> *NewFrames,unsigned int GlobalDuration);
	};
	
	class AnimationInstance{
		private:
			double LastUpdate;
			unsigned int CurrentFrame;
			
		public:
			Animation *Anim;
			
			AnimationInstance(Animation *NewAnim = nullptr);
			
			bool Update();
			void Reset();
	};
	
	#define ANIMATION_INC
#endif