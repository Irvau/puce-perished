#ifndef ENTITY_INC
	#include <vector>
	
	#include "Scene.hpp"
	
	enum EntityType{
		PINKAMENA_ENTITY,
		
		ENTITY_TYPE_COUNT
	};
	
	class Entity{
		friend class Scene;
		
		protected:
			EntityType Type;
			float X,Y;
			
		public:
			Entity(EntityType NewType,float NewX,float NewY)
			:Type(NewType),X(NewX),Y(NewY){
				
			}
			
			virtual void Update(Scene *ContainingScene) = 0;
			
			virtual float GetRadius() const = 0;
	};
	
	#define ENTITY_INC
#endif