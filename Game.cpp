#include <cstdlib>
#include <cstdio>

#include "Game.hpp"
#include "Shader.hpp"
#include "Tile.hpp"
#include "Resources.hpp"
#include "Utils.hpp"

Game Game::PP;

void Game::KeyCallback(GLFWwindow* Window,int Key,int Scancode,int Action,int Mods){
	if(Key == GLFW_KEY_ESCAPE){
		glfwSetWindowShouldClose(Window,GLFW_TRUE);
	}
	
	Game::PP.CurrentScene->CameraXAngle += ((float)(Key == GLFW_KEY_DOWN) - (float)(Key == GLFW_KEY_UP)) * GetDT();
	Game::PP.CurrentScene->CameraYAngle += ((float)(Key == GLFW_KEY_RIGHT) - (float)(Key == GLFW_KEY_LEFT)) * GetDT();
}

void Game::MouseButtonCallback(GLFWwindow* Window,int Button,int Action,int Mods){
	
}

Game::Game()
:CurrentScene(nullptr){
	
}

int Game::Run(){
	//Windows-stuffs
	glfwInit();
	
	const GLFWvidmode *CurrMode = glfwGetVideoMode(glfwGetPrimaryMonitor());
	glfwWindowHint(GLFW_RED_BITS,CurrMode->redBits);
	glfwWindowHint(GLFW_GREEN_BITS,CurrMode->greenBits);
	glfwWindowHint(GLFW_BLUE_BITS,CurrMode->blueBits);
	glfwWindowHint(GLFW_REFRESH_RATE,CurrMode->refreshRate);
	glfwWindowHint(GLFW_SAMPLES,2);
	
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR,3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR,3);
	glfwWindowHint(GLFW_OPENGL_PROFILE,GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_RESIZABLE,GL_FALSE);
	glfwWindowHint(GLFW_FOCUSED,GL_TRUE);
	
	GLFWwindow *Window = glfwCreateWindow(CurrMode->width,CurrMode->height,"Puce Perished v0.1",glfwGetPrimaryMonitor(),nullptr);
	
	if(Window == nullptr){
		printf("Window creation failed.\n");
		return EXIT_FAILURE;
	}
	
	glfwSetKeyCallback(Window,KeyCallback);
	glfwSetMouseButtonCallback(Window,MouseButtonCallback);
	
	//Initializing OpenGL stuffs
	glfwMakeContextCurrent(Window);
	
	glewExperimental = GL_TRUE;
	
	if(glewInit() != GLEW_OK){
		printf("GLEW initialization failed.\n");
		return EXIT_FAILURE;
	}
	
	int WWidth,WHeight;
	glfwGetFramebufferSize(Window,&WWidth,&WHeight);
	glViewport(0,0,WWidth,WHeight);
	
	glEnable(GL_MULTISAMPLE);  
	glEnable(GL_DEPTH_TEST);
	
	/*Culling, yo. Not sure if it's going to be practical in this project, but jic.
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	glFrontFace(GL_CCW);*/
	
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
	
	//Some more initialization
	Resources::STUFFS.Load();
	
	UseShader(Resources::STUFFS.RetrieveShader(MESH_SHADER));
	
	CurrentScene = new Scene(5,5);
	
	std::vector<int> NewTiles = {
		0,1, 1,0, 0,1, 1,0, 0,1,
		1,0, 0,1, 1,0, 0,1, 1,0,
		0,0, 1,0, 0,0, 1,0, 0,0,
		1,0,-1,0,-1,0,-1,0, 1,0,
		0,0, 1,0, 0,0, 1,0, 0,0
	};
	
	CurrentScene->SetTiles(&NewTiles);
	
	//To run!
	double MouseX = 0,MouseY = 0;
	
	while(!glfwWindowShouldClose(Window)){
		//Processing inputs
		glfwPollEvents();
		
		glfwGetCursorPos(Window,&MouseX,&MouseY);
		
		//Logics
		CurrentScene->Update();
		
		//Graphics
		glClearColor(0.0f,0.0f,0.0f,1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		
		CurrentScene->Draw();
		
		glfwSwapBuffers(Window);
		
		//Delta-times
		UpdateDT();
	}
	
	Resources::STUFFS.Free();
	
	glfwDestroyWindow(Window);
	
	glfwTerminate();
	
	return EXIT_SUCCESS;
}