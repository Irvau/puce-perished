#ifndef GAME_INC
	#include <GL/glew.h>
	#include <GLFW/glfw3.h>
	
	#include "Scene.hpp"
	
	class Game{
		private:
			static void KeyCallback(GLFWwindow* Window,int Key,int Scancode,int Action,int Mods);
			static void MouseButtonCallback(GLFWwindow* Window,int Button,int Action,int Mods);
			
		public:
			static Game PP;
			
			Scene *CurrentScene;
			
			Game();
			
			int Run();
	};
	
	#define GAME_INC
#endif