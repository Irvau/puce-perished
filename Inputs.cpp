#include "Inputs.hpp"

Inputs::Inputs(){
	Reset();
}

void Inputs::Set(InputType Index,InputState NewValue){
	States[Index] = NewValue;
}

InputState Inputs::GetState(InputType Index) const{
	return States[Index];
}

void Inputs::CopyFrom(Inputs *OtherInput,bool PressedOnly){
	if(OtherInput){
		for(int I = 0;I < INPUT_TYPE_COUNT;++I){
			if((!PressedOnly) || (PressedOnly && OtherInput->States[I] > States[I])){
				States[I] = OtherInput->States[I];
			}
		}
	}
}

void Inputs::Reset(){
	for(int I = 0;I < INPUT_TYPE_COUNT;++I){
		States[I] = NORMAL_INPUT_STATE;
	}
}