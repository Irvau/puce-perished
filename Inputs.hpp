#ifndef INPUTS_INC
	enum InputType{
		UP_INPUT,
		DOWN_INPUT,
		RIGHT_INPUT,
		LEFT_INPUT,
		
		DOTHING_INPUT,
		
		CONTINUE_STORY_INPUT,
		NEW_STORY_INPUT,
		
		NEW_SURVIVAL_INPUT,
		SURVIVAL_HIGHS_INPUT,
		
		MUTE_MUSIC_INPUT,
		MUTE_AUDIO_INPUT,
		
		INPUT_TYPE_COUNT
	};
	
	enum InputState{
		NORMAL_INPUT_STATE,
		HOVER_INPUT_STATE,
		PRESSED_INPUT_STATE,
		
		INPUT_STATE_COUNT
	};
	
	class Inputs{
		private:
			InputState States[INPUT_TYPE_COUNT];
			
		public:
			Inputs();
			
			void Set(InputType Index,InputState NewValue);
			InputState GetState(InputType Index) const;
			
			void CopyFrom(Inputs *OtherInput,bool PressedOnly = true);
			void Reset();
	};
	
	#define INPUTS_INC
#endif