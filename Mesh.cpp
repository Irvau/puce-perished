#include "Mesh.hpp"

Mesh::Mesh()
:VBO(0){
	
}

Mesh::~Mesh(){
	Free();
}

bool Mesh::Load(std::vector<GLfloat> *Data){
	if(Data && !(Data->size() % 5) && !VBO){
		glGenBuffers(1,&VBO);
		glBindBuffer(GL_ARRAY_BUFFER,VBO);
		glBufferData(GL_ARRAY_BUFFER,Data->size() * sizeof(float),Data->data(),GL_STATIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER,0);
		
		return true;
	}
	
	return false;
}

bool Mesh::Free(){
	if(VBO){
		glDeleteBuffers(1,&VBO);
		
		return true;
	}
	
	return false;
}

GLuint Mesh::GetVBO() const{
	return VBO;
}