#ifndef MESH_INC
	#include <vector>
	
	#include <GL/glew.h>
	
	class Mesh{
		private:
			GLuint VBO;
			
		public:
			Mesh();
			~Mesh();
			
			bool Load(std::vector<GLfloat> *Data);
			bool Free();
			
			GLuint GetVBO() const;
	};
	
	#define MESH_INC
#endif