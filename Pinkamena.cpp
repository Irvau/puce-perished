#include "Tile.hpp"
#include "Inputs.hpp"
#include "Pinkamena.hpp"
#include "Utils.hpp"

const float PINKAMENA_SPEED = 3.2;

Pinkamena::Pinkamena(float InitX,float InitY)
:Entity(PINKAMENA_ENTITY,InitX,InitY){
	
}

void Pinkamena::Update(Scene *CS){
	X += ((CS->GetInputState(RIGHT_INPUT) == PRESSED_INPUT_STATE) - (CS->GetInputState(LEFT_INPUT) == PRESSED_INPUT_STATE)) * PINKAMENA_SPEED * GetDT();
	Y += ((CS->GetInputState(DOWN_INPUT) == PRESSED_INPUT_STATE) - (CS->GetInputState(UP_INPUT) == PRESSED_INPUT_STATE)) * PINKAMENA_SPEED * GetDT();
}

float Pinkamena::GetRadius() const{
	return 1.2;
}