#ifndef PINKAMENA_INC
	#include "Scene.hpp"
	#include "Entity.hpp"
	
	class Pinkamena: public Entity{
		private:
			
		public:
			Pinkamena(float InitX,float InitY);
			
			virtual void Update(Scene *CS);
			
			virtual float GetRadius() const;
	};
	
	#define PINKAMENA_INC
#endif