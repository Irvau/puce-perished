#**"Puce Perished"**#

A 2d top-down game involving a sad pink horse and a lot of pseudo 4th wall breaking.

**Current version:** v0.01 (currently rebuilding everything found [here](https://bitbucket.org/Irvau/puce-perished-sfml-version))

A thread for this game is located [here](http://mylittlegamedev.com/thread-1530.html), complete with compiled versions available for download, personal rantings of mine, and community feedback.