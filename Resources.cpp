#include <cstdio>

#include "Resources.hpp"
#include "Shader.hpp"
#include "Tile.hpp"

Resources Resources::STUFFS;

Resources::Resources()
:IsLoaded(false){
	Shaders = new GLuint[SHADER_RESOURCE_COUNT];
	
	for(int I = 0;I < SHADER_RESOURCE_COUNT;++I){
		Shaders[I] = 0;
	}
	
	Textures = new Texture[TEXTURE_RESOURCE_COUNT];
	Meshes = new Mesh[MESH_RESOURCE_COUNT];
}

Resources::~Resources(){
	Free();
	
	delete[] Shaders;
	delete[] Textures;
	delete[] Meshes;
}

bool Resources::Loaded(){
	return IsLoaded;
}

bool Resources::Load(){
	if(!IsLoaded){
		//Shaders
		Shaders[MESH_SHADER] = CompileShader("Shaders/Mesh.vert.glsl","Shaders/Mesh.frag.glsl");
		
		//Textures
		Textures[TILES_TEXTURE].Path = "Textures/Tiles.png";
		
		for(int I = 0;I < TEXTURE_RESOURCE_COUNT;++I){
			Textures[I].Load();
		}
		
		float BorderDist = 1.0 / (float)TILE_TEX_DIMENSIONS;
		
		//Meshes
		std::vector<GLfloat> FlatTileVerts = {
			0.0,0.0,1.0,/**/BorderDist,1.0 - BorderDist,
			1.0,0.0,1.0,/**/1.0 - BorderDist,1.0 - BorderDist,
			0.0,0.0,0.0,/**/BorderDist,BorderDist,
			1.0,0.0,0.0,/**/1.0 - BorderDist,BorderDist
		};
		
		Meshes[FLAT_TILE_MESH].Load(&FlatTileVerts);
		
		std::vector<GLfloat> TallTileVerts = {
			0.0,0.0,1.0,/**/BorderDist,2.0 - BorderDist,
			1.0,0.0,1.0,/**/1.0 - BorderDist,2.0 - BorderDist,
			0.0,1.0,1.0,/**/BorderDist,1.0,
			
			1.0,1.0,1.0,/**/1.0 - BorderDist,1.0,
			0.0,1.0,0.0,/**/BorderDist,BorderDist,
			1.0,1.0,0.0,/**/1.0 - BorderDist,BorderDist
		};
		
		Meshes[TALL_TILE_MESH].Load(&TallTileVerts);
		
		std::vector<GLfloat> EntityVerts = {
			0.0,0.0,0.0,/**/BorderDist,BorderDist,
			1.0,0.0,0.0,/**/1.0 - BorderDist,BorderDist,
			0.0,1.0,0.0,/**/BorderDist,1.0 - BorderDist,
			1.0,1.0,0.0,/**/1.0 - BorderDist,1.0 - BorderDist
		};
		
		Meshes[ENTITY_MESH].Load(&EntityVerts);
		
		IsLoaded = true;
		
		return true;
	}
	
	return false;
}

bool Resources::Free(){
	if(IsLoaded){
		for(int I = 0;I < SHADER_RESOURCE_COUNT;++I){
			if(Shaders[I]){
				glDeleteProgram(Shaders[I]);
				Shaders[I] = 0;
			}
		}
		
		for(int I = 0;I < TEXTURE_RESOURCE_COUNT;++I){
			Textures[I].Free();
		}
		
		for(int I = 0;I < MESH_RESOURCE_COUNT;++I){
			Meshes[I].Free();
		}
		
		IsLoaded = false;
		
		return true;
	}
	
	return false;
}

GLuint Resources::RetrieveShader(ShaderResource RequestedShader){
	return Shaders[RequestedShader];
}

const Texture *Resources::RetrieveTexture(TextureResource RequestedTexture){
	return &Textures[RequestedTexture];
}

const Mesh *Resources::RetrieveMesh(MeshResource RequestedMesh){
	return &Meshes[RequestedMesh];
}