#ifndef RESOURCES_INC
	#include <vector>
	
	#include <GL/glew.h>
	
	#include "Texture.hpp"
	#include "Mesh.hpp"
	
	enum ShaderResource{
		MESH_SHADER,
		
		SHADER_RESOURCE_COUNT
	};
	
	enum TextureResource{
		TILES_TEXTURE,
		
		TEXTURE_RESOURCE_COUNT
	};
	
	enum MeshResource{
		FLAT_TILE_MESH,
		TALL_TILE_MESH,
		ENTITY_MESH,
		
		MESH_RESOURCE_COUNT
	};
	
	class Resources{
		private:
			bool IsLoaded;
			
			GLuint *Shaders;
			Texture *Textures;
			Mesh *Meshes;
			
		public:
			static Resources STUFFS;
			
			Resources();
			~Resources();
			
			bool Loaded();
			
			bool Load();
			bool Free();
			
			GLuint RetrieveShader(ShaderResource RequestedShader);
			const Texture *RetrieveTexture(TextureResource RequestedTexture);
			const Mesh *RetrieveMesh(MeshResource RequestedMesh);
	};
	
	#define RESOURCES_INC
#endif