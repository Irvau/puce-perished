#include <cmath>
#include <algorithm>
#include <cstdio>

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "Scene.hpp"
#include "Entity.hpp"
#include "Utils.hpp"
#include "Resources.hpp"
#include "Shader.hpp"

const float VIEW_WIDTH = 10.0,VIEW_HEIGHT = 8.0;

std::vector<Scene*> Scenes;

Scene::Scene(unsigned int NewWidth,unsigned int NewHeight)
:DunDidUpdate(false),Width(NewWidth),Height(NewHeight),CameraX(Width * 0.5),CameraY(Height * 0.5),CameraXAngle(45.0),CameraYAngle(0.0),
FlatTilesVAO(0),FlatTilesVBO(0),FlatTilesCount(0),TallTilesVAO(0),TallTilesVBO(0),TallTilesCount(0){
	Scenes.push_back(this);
}

Scene::~Scene(){
	Scenes.erase(std::remove(Scenes.begin(),Scenes.end(),this),Scenes.end());
}

void Scene::Update(){
	//Entity updates
	for(std::vector<Entity*>::iterator IT = Entities.begin();IT != Entities.end();++IT){
		(*IT)->Update(this);
	}
	
	//Entity collisions
	for(std::vector<Entity*>::iterator IT = Entities.begin();IT != Entities.end();++IT){
		//First with tiles
		for(int I = 0;I < Tiles.size();++I){
			if(Tiles[I] && Tiles[I]->IsTall && EntityIn(*IT,I)){
				float Distances[] = {
					std::numeric_limits<float>::infinity(),
					(*IT)->X + (*IT)->GetRadius() - (I % Width),
					(*IT)->Y - std::floor(I / Width),
					(*IT)->X - (*IT)->GetRadius() - (I % Width) - 1,
					(*IT)->Y - std::floor(I / Width) - 1
				};
				
				int ClosestDistance = 0;
				
				for(int I = 1;I < 5;++I){
					if(std::abs(Distances[I]) < std::abs(Distances[ClosestDistance])){
						ClosestDistance = I;
					}
				}
				
				if(ClosestDistance){
					float Movement = -1 * Distances[ClosestDistance];
					
					if(ClosestDistance % 2){
						(*IT)->X += Movement;
					}else{
						(*IT)->Y += Movement;
					}
				}
			}
		}
		
		//Then with scene boundaries
		LimitVar((*IT)->X,0,Width);
		LimitVar((*IT)->Y,0,Height);
	}
	
	//Ta-da
	DunDidUpdate = true;
}

void Scene::Draw(){
	if((!Tiles.empty()) || (!Entities.empty())){
		glm::mat4 Projection = glm::ortho(VIEW_WIDTH * -0.5f,VIEW_WIDTH * 0.5f,VIEW_HEIGHT * -0.5f,VIEW_HEIGHT * 0.5f,0.01f,20.f);
		
		glm::mat4 View;
		View = glm::translate(View,glm::vec3(0.0,0.0f,-1 * 5));
		View = glm::rotate(View,CameraXAngle,glm::vec3(1.0,0.0,0.0));
		View = glm::rotate(View,CameraYAngle,glm::vec3(0.0,1.0,0.0));
		
		glm::mat4 Model;
		Model = glm::translate(Model,glm::vec3(-1 * CameraX,0.0f,-1 * CameraY));
		
		glm::mat4 PVM = Projection * View * Model;
		
		UseShader(Resources::STUFFS.RetrieveShader(MESH_SHADER));
		glUniformMatrix4fv(glGetUniformLocation(GetCurrentShader(),"PVM"),1,GL_FALSE,glm::value_ptr(PVM));
	}
	
	if(!Tiles.empty()){
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D,Resources::STUFFS.RetrieveTexture(TILES_TEXTURE)->GetID());
		glUniform1i(glGetUniformLocation(GetCurrentShader(),"Tex"),0);
		
		Vecti Dims = Resources::STUFFS.RetrieveTexture(TILES_TEXTURE)->GetDimensions();
		
		glUniform2i(glGetUniformLocation(GetCurrentShader(),"TextureDimensions"),Dims.X,Dims.Y);
		glUniform2i(glGetUniformLocation(GetCurrentShader(),"TextureDivisions"),2,2);
		
		if(FlatTilesVAO){
			glBindVertexArray(FlatTilesVAO);
			glDrawArraysInstanced(GL_TRIANGLE_STRIP,0,4,FlatTilesCount);
		}
		
		if(TallTilesVAO){
			glBindVertexArray(TallTilesVAO);
			glDrawArraysInstanced(GL_TRIANGLE_STRIP,0,6,TallTilesCount);
		}
	}
}

bool Scene::EntityIn(Entity *Target,unsigned int TileIndex){
	if(TileIndex < Width * Height && Tiles[TileIndex]){
		if(Target->Y >= std::floor(TileIndex / Width) && Target->Y <= std::floor(TileIndex / Width) + 1){
			if(Target->X - Target->GetRadius() >= TileIndex % Width && Target->X + Target->GetRadius() <= (TileIndex % Width) + 1){
				return true;
			}
		}
	}
	
	return false;
}

bool Scene::SetTiles(std::vector<int> *NewTiles){
	if(NewTiles && NewTiles->size() == Width * Height * 2){
		//Clearing the old stuff
		if(!Tiles.empty()){
			for(std::vector<Tile*>::iterator IT = Tiles.begin();IT != Tiles.end();++IT){
				if(*IT){
					delete (*IT);
				}
			}
			
			Tiles.clear();
		}
		
		glDeleteVertexArrays(1,&FlatTilesVAO);
		glDeleteBuffers(1,&FlatTilesVBO);
		glDeleteVertexArrays(1,&TallTilesVAO);
		glDeleteBuffers(1,&TallTilesVBO);
		
		//Loading in the new tiles
		for(std::vector<int>::iterator IT = NewTiles->begin();IT < NewTiles->end();IT += 2){
			if(*IT < 0){
				Tiles.push_back(nullptr);
			}else{
				Tile *NewTile = new Tile(*IT,*(IT + 1) > 0);
				Tiles.push_back(NewTile);
			}
		}
		
		//Generating the instanced VAOs for the tiles
		std::vector<GLfloat> FlatTilePositions,TallTilePositions;
		
		for(int I = 0;I < Tiles.size();++I){
			if(Tiles[I]){
				if(Tiles[I]->IsTall){
					TallTilePositions.push_back(Tiles[I]->ID);
					TallTilePositions.push_back(I % Width);
					TallTilePositions.push_back(std::floor(I / Width));
					
					++TallTilesCount;
				}else{
					FlatTilePositions.push_back(Tiles[I]->ID);
					FlatTilePositions.push_back(I % Width);
					FlatTilePositions.push_back(std::floor(I / Width));
					
					++FlatTilesCount;
				}
			}
		}
		
		if(!FlatTilePositions.empty()){
			glGenVertexArrays(1,&FlatTilesVAO);
			glBindVertexArray(FlatTilesVAO);
			
			glBindBuffer(GL_ARRAY_BUFFER,Resources::STUFFS.RetrieveMesh(FLAT_TILE_MESH)->GetVBO());
			
			glVertexAttribPointer(0,3,GL_FLOAT,GL_FALSE,5 * sizeof(GLfloat),(GLvoid*)0);
			glEnableVertexAttribArray(0);
			glVertexAttribPointer(1,2,GL_FLOAT,GL_FALSE,5 * sizeof(GLfloat),(GLvoid*)(3 * sizeof(GLfloat)));
			glEnableVertexAttribArray(1);
			
			glGenBuffers(1,&FlatTilesVBO);
			glBindBuffer(GL_ARRAY_BUFFER,FlatTilesVBO);
			glBufferData(GL_ARRAY_BUFFER,FlatTilePositions.size() * sizeof(GLfloat),FlatTilePositions.data(),GL_STATIC_DRAW);
			
			glVertexAttribPointer(2,1,GL_FLOAT,GL_FALSE,3 * sizeof(GLfloat),(GLvoid*)0);
			glEnableVertexAttribArray(2);
			glVertexAttribPointer(3,2,GL_FLOAT,GL_FALSE,3 * sizeof(GLfloat),(GLvoid*)sizeof(GLfloat));
			glEnableVertexAttribArray(3);
			
			glVertexAttribDivisor(2,1);
			glVertexAttribDivisor(3,1);
		}
		
		if(!TallTilePositions.empty()){
			glGenVertexArrays(1,&TallTilesVAO);
			glBindVertexArray(TallTilesVAO);
			
			glBindBuffer(GL_ARRAY_BUFFER,Resources::STUFFS.RetrieveMesh(TALL_TILE_MESH)->GetVBO());
			
			glVertexAttribPointer(0,3,GL_FLOAT,GL_FALSE,5 * sizeof(GLfloat),(GLvoid*)0);
			glEnableVertexAttribArray(0);
			glVertexAttribPointer(1,2,GL_FLOAT,GL_FALSE,5 * sizeof(GLfloat),(GLvoid*)(3 * sizeof(GLfloat)));
			glEnableVertexAttribArray(1);
			
			glGenBuffers(1,&TallTilesVBO);
			glBindBuffer(GL_ARRAY_BUFFER,TallTilesVBO);
			glBufferData(GL_ARRAY_BUFFER,TallTilePositions.size() * sizeof(GLfloat),TallTilePositions.data(),GL_STATIC_DRAW);
			
			glVertexAttribPointer(2,1,GL_FLOAT,GL_FALSE,3 * sizeof(GLfloat),(GLvoid*)0);
			glEnableVertexAttribArray(2);
			glVertexAttribPointer(3,2,GL_FLOAT,GL_FALSE,3 * sizeof(GLfloat),(GLvoid*)sizeof(GLfloat));
			glEnableVertexAttribArray(3);
			
			glVertexAttribDivisor(2,1);
			glVertexAttribDivisor(3,1);
		}
		
		glBindVertexArray(0);
		glBindBuffer(GL_ARRAY_BUFFER,0);
		
		return true;
	}
	
	return false;
}

unsigned int Scene::GetWidth() const{
	return Width;
}

unsigned int Scene::GetHeight() const{
	return Height;
}

void Scene::GetDimensions(int *WidthVar,int *HeightVar) const{
	*WidthVar = Width;
	*HeightVar = Height;
}

InputState Scene::GetInputState(InputType Type) const{
	return In.GetState(Type);
}

void AllowSceneUpdasions(){
	for(std::vector<Scene*>::iterator IT = Scenes.begin();IT != Scenes.end();++IT){
		(*IT)->DunDidUpdate = false;
		(*IT)->In.Reset();
	}
}