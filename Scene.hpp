#ifndef SCENE_INC
	#include <vector>
	
	#include <GL/glew.h>
	
	#include "Inputs.hpp"
	#include "Tile.hpp"
	
	class Entity;
	
	class Scene{
		friend class Game;
		
		friend void AllowSceneUpdasions();
		
		private:
			Inputs In;
			bool DunDidUpdate;
			
			unsigned int Width,Height;
			
			float CameraX,CameraY;
			float CameraXAngle,CameraYAngle;
			
			std::vector<Tile*> Tiles;
			GLuint FlatTilesVAO,FlatTilesVBO,FlatTilesCount;
			GLuint TallTilesVAO,TallTilesVBO,TallTilesCount;
			
			std::vector<Entity*> Entities;
			
		public:
			Scene(unsigned int NewWidth,unsigned int NewHeight);
			~Scene();
			
			void Update();
			void Draw();
			
			bool EntityIn(Entity *Target,unsigned int TileIndex);
			
			bool SetTiles(std::vector<int> *NewTiles);
			
			unsigned int GetWidth() const;
			unsigned int GetHeight() const;
			void GetDimensions(int *WidthVar,int *HeightVar) const;
			
			InputState GetInputState(InputType Type) const;
	};
	
	void AllowSceneUpdasions();
	
	#define SCENE_INC
#endif