#include <cstdio>
#include <algorithm>

#include "Scene.hpp"
#include "Screen.hpp"

unsigned int CameraTexDimensions = 0;

Screen::Screen(Scene *NewScene)
:FrameBuff(0),RenderTexture(0),RenderBuff(0),CurrScene(nullptr),X(0),Y(0){
	glGenFramebuffers(1,&FrameBuff);
	glBindFramebuffer(GL_FRAMEBUFFER,FrameBuff);  
	
	glGenTextures(1,&RenderTexture);
	glBindTexture(GL_TEXTURE_2D,RenderTexture);
	
	glTexImage2D(GL_TEXTURE_2D,0,GL_RGB,CameraTexDimensions,CameraTexDimensions,0,GL_RGB,GL_UNSIGNED_BYTE,0);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	
	glFramebufferTexture2D(GL_FRAMEBUFFER,GL_COLOR_ATTACHMENT0,GL_TEXTURE_2D,RenderTexture,0);
	
	glGenRenderbuffers(1,&RenderBuff);
	glBindRenderbuffer(GL_RENDERBUFFER,RenderBuff); 
	glRenderbufferStorage(GL_RENDERBUFFER,GL_DEPTH24_STENCIL8,CameraTexDimensions,CameraTexDimensions);
	glBindRenderbuffer(GL_RENDERBUFFER,0);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER,GL_DEPTH_STENCIL_ATTACHMENT,GL_RENDERBUFFER,RenderBuff);
	
	if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE){
		printf("Framebuffer creation failed.\n");
	}
	
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

Screen::~Screen(){
	glDeleteFramebuffers(1,&FrameBuff);
	glDeleteTextures(1,&RenderTexture);
	glDeleteRenderbuffers(1,&RenderBuff);
}

void Screen::CalculateTexSize(unsigned int ScreenWidth,unsigned int ScreenHeight){
	CameraTexDimensions = std::min(ScreenWidth,ScreenHeight);
}

void Screen::Render(){
	if(CurrScene){
		glBindFramebuffer(GL_FRAMEBUFFER,FrameBuff);
		
		CurrScene->Draw();
		
		glBindFramebuffer(GL_FRAMEBUFFER,0);
	}
}

void Screen::SetTargetScene(Scene *NewScene){
	CurrScene = NewScene;
}

void Screen::SetPosition(float NewX,float NewY){
	X = NewX;
	Y = NewY;
}

GLuint Screen::GetRenderTexture(){
	return RenderTexture;
}

void Screen::SetInput(InputType Index,InputState NewState){
	In.Set(Index,NewState);
}