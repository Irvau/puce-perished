#ifndef CAMERA_INC
	#include <GL/glew.h>
	
	#include "Inputs.hpp"
	
	class Scene;
	
	class Screen{
		private:
			Inputs In;
			
			GLuint FrameBuff,RenderTexture,RenderBuff;
			Scene *CurrScene;
			
			float X,Y;
			
		public:
			Screen(Scene *NewScene = nullptr);
			~Screen();
			
			static void CalculateTexSize(unsigned int ScreenWidth,unsigned int ScreenHeight);
			
			void Update();
			void Render();
			void Draw();
			
			void SetTargetScene(Scene *NewScene);
			void SetPosition(float NewX,float NewY);
			GLuint GetRenderTexture();
			
			void SetInput(InputType Index,InputState NewState);
	};
	
	#define CAMERA_INC
#endif