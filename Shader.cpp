#include <string>
#include <fstream>
#include <sstream>
#include <iostream>

#include "Shader.hpp"

GLuint CurrentShader;

GLuint CompileShader(const GLchar *VertexPath,const GLchar *FragmentPath){
	//Reading the shader files
	std::ifstream VertFile,FragFile;
	std::stringstream VertStream,FragStream;
	std::string VertCodes,FragCodes;
	
	VertFile.exceptions(std::ifstream::badbit);
	FragFile.exceptions(std::ifstream::badbit);
	
	try{
		VertFile.open(VertexPath);
		FragFile.open(FragmentPath);
		
		std::stringstream VertStream,FragStream;
		
		VertStream << VertFile.rdbuf();
		FragStream << FragFile.rdbuf();
		
		VertFile.close();
		FragFile.close();
		
		VertCodes = VertStream.str();
		FragCodes = FragStream.str();
	}catch(std::ifstream::failure E){
		printf("Error reading shader file\n");
	}
	
	const GLchar *VertChars = VertCodes.c_str();
	const GLchar *FragChars = FragCodes.c_str();
	
	//Compiling them
	GLuint Vert,Frag;
	GLint Status;
	GLchar Log[512];
	
	//Vertex
	Vert = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(Vert,1,&VertChars,nullptr);
	glCompileShader(Vert);
	
	glGetShaderiv(Vert,GL_COMPILE_STATUS,&Status);
	
	if(!Status){
		glGetShaderInfoLog(Vert,512,nullptr,Log);
		printf("(\"%s\") Shader compilation failed: %s\n",VertexPath,Log);
	}
	
	//Fragment
	Frag = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(Frag,1,&FragChars,nullptr);
	glCompileShader(Frag);
	
	glGetShaderiv(Frag,GL_COMPILE_STATUS,&Status);
	
	if(!Status){
		glGetShaderInfoLog(Frag,512,nullptr,Log);
		printf("(\"%s\") Shader compilation failed: %s\n",FragmentPath,Log);
	}
	
	//The entire shader program
	GLuint ID = glCreateProgram();
	glAttachShader(ID,Vert);
	glAttachShader(ID,Frag);
	glLinkProgram(ID);
	
	glGetProgramiv(ID,GL_LINK_STATUS,&Status);
	
	if(!Status){
		glGetProgramInfoLog(ID,512,nullptr,Log);
		printf("(\"%s\" & \"%s\")Shader linking failed: %s\n",VertexPath,FragmentPath,Log);
	}else{
		printf("Successfully compiled shader with sources \"%s\" and \"%s\"\n",VertexPath,FragmentPath);
	}
	
	//Cleaning up some stuffs
	glDeleteShader(Vert);
	glDeleteShader(Frag);
	
	//Ta-da!
	return ID;
}

void UseShader(GLuint NewShader){
	if(CurrentShader != NewShader){
		CurrentShader = NewShader;
		glUseProgram(CurrentShader);
	}
}

GLuint GetCurrentShader(){
	return CurrentShader;
}