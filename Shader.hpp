#ifndef SHADER_INC
	#include <GL/glew.h>
	
	GLuint CompileShader(const GLchar *VertexPath,const GLchar *FragmentPath);
	
	void UseShader(GLuint NewShader);
	GLuint GetCurrentShader();
	
	#define SHADER_INC
#endif