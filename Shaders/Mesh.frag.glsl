#version 330 core

uniform sampler2D Tex;

in vec2 TexCoords;
out vec4 OutColor;

void main(){
	OutColor = texture(Tex,TexCoords);
}