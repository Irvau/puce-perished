#version 330 core

layout (location = 0) in vec3 Position;
layout (location = 1) in vec2 UV;
layout (location = 2) in float TextureID;
layout (location = 3) in vec2 Offset;

uniform mat4 PVM;

uniform ivec2 TextureDimensions = ivec2(0,0);
uniform ivec2 TextureDivisions = ivec2(0,0);

out vec2 TexCoords;

void main(){
	clamp(TextureID,0,TextureDivisions.x * TextureDivisions.y - 1);
	
	TexCoords = (UV + vec2(mod(TextureID,TextureDivisions.x),floor(TextureID / TextureDivisions.x))) * (TextureDimensions / TextureDivisions) / TextureDimensions;
	gl_Position = PVM * vec4(Position + vec3(Offset.x,0,Offset.y),1.0f);
}