#include <algorithm>
#include <cstdio>

#include "SOIL/SOIL.h"

#include "Texture.hpp"

const std::string ALPHA_TEXTURE_EXTENSION = ".png";

Texture::Texture(std::string NewPath)
:ID(0),Dimensions(0,0),Path(NewPath){
	
}

Texture::~Texture(){
	Free();
}

bool Texture::Load(){
	if(!ID && Path.size() > 4){
		glGenTextures(1,&ID);
		glBindTexture(GL_TEXTURE_2D,ID);
		
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST);
		
		unsigned char *Image;
		
		if(std::equal(Path.begin() + Path.size() - ALPHA_TEXTURE_EXTENSION.size(),Path.end(),ALPHA_TEXTURE_EXTENSION.begin())){
			Image = SOIL_load_image(Path.c_str(),&Dimensions.X,&Dimensions.Y,nullptr,SOIL_LOAD_RGBA);
			glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA,Dimensions.X,Dimensions.Y,0,GL_RGBA,GL_UNSIGNED_BYTE,Image);
		}else{
			Image = SOIL_load_image(Path.c_str(),&Dimensions.X,&Dimensions.Y,nullptr,SOIL_LOAD_RGB);
			glTexImage2D(GL_TEXTURE_2D,0,GL_RGB,Dimensions.X,Dimensions.Y,0,GL_RGB,GL_UNSIGNED_BYTE,Image);
		}
		
		SOIL_free_image_data(Image);
		glBindTexture(GL_TEXTURE_2D,0);
		
		printf("Loaded texture \"%s\"\n",Path.c_str());
		
		return true;
	}
	
	return false;
}

bool Texture::Free(){
	if(ID){
		glDeleteTextures(1,&ID);
		
		ID = 0;
		Dimensions.X = 0;
		Dimensions.Y = 0;
		
		return true;
	}
	
	return false;
}

GLuint Texture::GetID() const{
	return ID;
}

Vecti Texture::GetDimensions() const{
	return Dimensions;
}