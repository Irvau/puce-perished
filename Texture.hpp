#ifndef TEXTURE_INC
	#include <string>
	
	#include <GL/glew.h>
	
	#include "Utils.hpp"
	
	class Texture{
		protected:
			GLuint ID;
			Vecti Dimensions;
			
		public:
			std::string Path;
			
			Texture(std::string NewPath = "");
			~Texture();
			
			bool Load();
			bool Free();
			
			GLuint GetID() const;
			Vecti GetDimensions() const;
	};
	
	#define TEXTURE_INC
#endif