#ifndef TILE_INC
	const int TILE_TEX_DIMENSIONS = 32;
	
	struct Tile{
		int ID;
		float Opacity;
		bool IsTall;
		
		Tile(int InitID,bool InitTall = false)
		:ID(InitID),Opacity(1.0),IsTall(InitTall){
			
		}
	};
	
	#define TILE_INC
#endif