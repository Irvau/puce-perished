#include <GLFW/glfw3.h>

#include "Utils.hpp"

double DeltaTime = 0.0,LastTime = 0.0;

void UpdateDT(){
	DeltaTime = glfwGetTime() - LastTime;
	LastTime = glfwGetTime();
	
	if(DeltaTime > 1){
		DeltaTime = 1;
	}
}

float GetDT(){
	return DeltaTime;
}