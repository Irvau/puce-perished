#ifndef UTILS_INC
	#include <cstdlib>
	#include <ctime>
	#include <sstream>
	#include <vector>
	
	template<class T>
	struct Vect{
		T X,Y;
		
		Vect(){
		
		}
		
		Vect(T InitX,T InitY)
		:X(InitX),Y(InitY){
			
		}
	};
	
	using Vectu = Vect<unsigned int>;
	using Vecti = Vect<int>;
	using Vectf = Vect<float>;
	
	void UpdateDT();
	float GetDT();
	
	template<typename T,typename U,typename V>
	inline void LimitVar(T &Limitee,U Min,V Max){
		Limitee += ((Limitee < Min) * (Min - Limitee)) - ((Limitee > Max) * (Limitee - Max));
	}
	
	inline int SuperRands(){
		srand(time(nullptr));
		return rand();
	}
	
	template<typename T>
	std::string ToString(const T &Value){
		std::ostringstream Stream;
		Stream << Value;
		
		return Stream.str();
	}
	
	#define UTILS_INC
#endif